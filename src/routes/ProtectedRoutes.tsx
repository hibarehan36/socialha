import { Navigate } from "react-router-dom";

const ProtectedRoutes = (props: { children: JSX.Element }) => {
  const { children } = props;
  return (
    <>
      {!localStorage.getItem("token") && <Navigate to="/login" replace />}
      {children}
    </>
  );
};

export default ProtectedRoutes;
