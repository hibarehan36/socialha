import Home from "../views/Home";
import ProtectedRoutes from "./ProtectedRoutes";
import Login from "../views/Login";
import MainLayout from "../layout/MainLayout";


const MainRoutes = [
  {
    path: "/",
    element: (
      <ProtectedRoutes>
        <MainLayout />
      </ProtectedRoutes>
    ),
    children: [
      {
        path: "/",
        element: <Home />,
        index: true,
      },
      {
        path: "/managements",
        element: <Home />,
        index: true,
      },
      {
        path: "/statistics",
        element: <Home />,
        index: true,
      },
    ],
  },

  {
    path: "/login",
    element: <Login />,
  },
];
export default MainRoutes;
