import React from "react";
import { Typography, Box, Avatar } from "@mui/material";
import { GridRenderCellParams, GridActionsCellItem } from "@mui/x-data-grid";
import {
  deleteIconSrc,
  editIconSrc,
  viewIconSrc,
} from "../context/imageProvider";
import { getImageName } from "../utils";
import { handelActionType } from "../types";

export const UseGridRenderCell = (handelAction: handelActionType) => {
  const renderName = (params: GridRenderCellParams<any, Date>) => (
    <Box display="flex" alignItems="center">
      <Avatar  sizes="small" alt="" src={params.row.personImage} className="post-grid-cell-avatar" />
      <Typography variant="subtitle1"  ml={1} className="post-grid-cell-text1">
        {params.row.name}
      </Typography>
    </Box>
  );
  const renderActions = (params: GridRenderCellParams<any, Date>) => (
    <>
      <GridActionsCellItem
        icon={<img src={viewIconSrc} width={23}  alt="" />}
        label=""
        onClick={() => handelAction({ row: params.row, actionType: "view" })}
      />
      <GridActionsCellItem
        icon={<img src={editIconSrc} width={30}  alt="" />}
        label=""
        onClick={() => handelAction({ row: params.row, actionType: "edit" })}
      />
      <GridActionsCellItem
        icon={<img src={deleteIconSrc} width={20} alt="" />}
        label=""
        onClick={() => handelAction({ row: params.row, actionType: "delete" })}
      />
    </>
  );

  const renderDefault = (params: GridRenderCellParams<any, Date>) => (
    <Typography variant="subtitle2" className="post-grid-cell-text">
      {params.row[params.field]}
    </Typography>
  );
  const renderMediaUrl = (params: GridRenderCellParams<any, Date>) => (
    <Typography variant="subtitle2" className="post-grid-cell-text">
      {getImageName(params.row[params.field])}
    </Typography>
  );
  const renderCell = (
    params: GridRenderCellParams<any, Date>,
    name?: string
  ) => {
    switch (params.field) {
      case "actions":
        return renderActions(params);
      case "name":
        return renderName(params);
      case "mediaUrl":
        return renderMediaUrl(params);
      default:
        return renderDefault(params);
    }
  };

  return renderCell;
};
