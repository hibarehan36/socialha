import { useState } from "react";
import { actionInfoType} from "../types";
export const UseHandelGridActions = () => {
  const [action, setAction] = useState<actionInfoType>({
    row: {
      id: 0,
      postId: "",
      name: "",
      personImage: "",
      content: "",
      mediaUrl: "",
      privacy: "public",
    },
    actionType: "view",
  });
  const [isOpen, setIsOpen] = useState<boolean>(false);
  // handle action to know which avtion accord and get the selected row 
  //this is to make the right behavior for every action 
  const handelAction = (actionInfo: actionInfoType) => {  
    setAction(actionInfo);
    setIsOpen(true);
  };
  // resetDefault after every handeling action process
  const resetDefault = () => {
    setIsOpen(false);
  };
  return { handelAction, action, isOpen, resetDefault };
};
