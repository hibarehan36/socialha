import { useMutation, useQuery, useQueryClient } from "react-query";
import { mapping } from "../utils";
//here I orgnize requests as a simulation to rtk query
const API_BASE_URL = "https://graphqlzero.almansi.me/api";

export const useGetHandelPostsApis = () => {
  const queryClient = useQueryClient();
  const queryKey = "posts";

  const fetchPosts = async () => {
    const response = await fetch(API_BASE_URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        query: `
        query {
            posts {
              data {
                id
                body
              }}}
        `,
      }),
    });
    const data = await response.json();
    return mapping(data);
  };
  const deletePost = async (postId: string) => {
    const response = await fetch(API_BASE_URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        query: `
          mutation($id: ID!) {
            deletePost(id: $id)
          }
        `,
        variables: {
          id: postId,
        },
      }),
    });

    const data = await response.json();
    return data;
  };

  const updatePost = async (variables: {
    postId: string;
    updatedPost: string;
  }) => {
    const response = await fetch(API_BASE_URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        query: `
        mutation (
          $id: ID!,
          $input: UpdatePostInput!
        ) {
          updatePost(id: $id, input: $input) {
            id
            body
          }
        }
        `,
        variables: {
          id: variables.postId,
          input: {
            body: variables.updatedPost,
          },
        },
      }),
    });

    const data = await response.json();
    return data;
  };

  const mutationOptions = {
    onSuccess: () => {
      queryClient.invalidateQueries(queryKey);
    },
  };
  const useFetchPosts = () => useQuery(queryKey, fetchPosts);
  const useDeletePost = () => useMutation(deletePost, mutationOptions);
  const useUpdatePost = () => useMutation(updatePost, mutationOptions);
  return {
    useFetchPosts,
    useDeletePost,
    useUpdatePost,
  };
};
