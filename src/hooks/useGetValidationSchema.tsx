import * as Yup from "yup";

export const useGetValidationSchema = () => {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  const loginValidationSchema = Yup.object({
    email: Yup.string()
      .matches(emailRegex, "Invalid email address")
      .required("Email is required"),
    password: Yup.string()
      .required("Password is required")
      .min(6, "Password must be at least 6 characters"),
  });

  const postValidationSchema = Yup.object({
    content: Yup.string()
      .required("content is required")
      .min(4, "content must be at least 4 characters"),
  });

  return { loginValidationSchema , postValidationSchema};
};
