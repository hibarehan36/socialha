import { UseGridRenderCell } from "./useGridRenderCell";
import { actionInfoType } from "../types";

export const UseGetGridColumns = (
  handelAction: (actionInfo: actionInfoType) => void
) => {
  const renderCell = UseGridRenderCell(handelAction);
  const gridColumns = [
    { field: "postId", headerName: "Post ID", flex: 1, renderCell: renderCell },
    {
      field: "name",
      headerName: "Name",
      flex: 1,
      renderCell: renderCell,
    },
    {
      field: "content",
      headerName: "Content",
      flex: 1,
      renderCell: renderCell,
    },
    {
      field: "mediaUrl",
      headerName: "Media URL",
      flex: 1,
      renderCell: renderCell,
    },
    {
      field: "privacy",
      headerName: "Privacy",
      width: 120,
      renderCell: renderCell,
    },
    {
      field: "actions",
      headerName: "Actions",
      type: "actions",
      sortable: false,
      flex: 1,
      renderCell: renderCell,
    },
  ];
  return gridColumns;
};
