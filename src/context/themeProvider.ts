import { createTheme } from "@mui/material/styles";
import colors from "../style/colors.module.scss";


const theme = createTheme({
  palette: {
    primary: {
      main: colors.primaryColor,
    },
    secondary: {
      main: colors.secondaryColor,
    },
    text: {
      primary: colors.textColor,
      secondary: colors.grayColor,
    },
    error:{
      main:colors.heartColor 
    }
  },
});

export default theme;
