import React from "react";
import Routes from "./routes";
import './App.css'
import { Box } from "@mui/material";
function App() {
  return (
    <Box className="App">
      <Routes />
    </Box>
  );
}

export default App;
