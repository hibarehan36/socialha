import React from "react";
import { Box, TextField } from "@mui/material";
import { InputAdornment } from "@mui/material";
import { searchIconSrc } from "../../context/imageProvider";
import { handelSearch } from "../../utils";
import { GenericSearshFieldPropsType } from "../../types";

const SearchField = (props: GenericSearshFieldPropsType) => {
    const {data , setFilterdRows} = props;
  return (
    <Box mb={2} ml={2} mt={-1} >
      <TextField
        type="text"
        placeholder="Type here"
        name="search"
        variant="outlined"
        className="search-field"
        onChange={(event)=>handelSearch(event , data , setFilterdRows)}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <img src={searchIconSrc} width={30} alt="" />
            </InputAdornment>
          ),
        }}
      />
    </Box>
  );
};

export default SearchField;
