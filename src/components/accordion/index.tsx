import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Box,
} from "@mui/material";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import { GenericAccordionPropsType } from "../../types";
const Accordioin = (props: GenericAccordionPropsType) => {
  const { children, expanded = true } = props;

  return (
    <Accordion
      color="text.dark"
      sx={{
        "&.MuiAccordion-root": {
          boxShadow: "none",
        },
      }}
      defaultExpanded={expanded}
    >
      <AccordionSummary expandIcon={<ExpandLessIcon />}>
        {children[0]}
      </AccordionSummary>
      <Box mt={-3}>  
      <AccordionDetails sx={{ padding: 0 }}>{children[1]}</AccordionDetails>
      </Box>
    </Accordion>
  );
};

export default Accordioin;
