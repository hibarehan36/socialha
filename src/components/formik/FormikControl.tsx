import { formikControlPropsType } from "../../types";
import ControlTexInput from "../inputs/ControlTexInput";


const FormokControl = (props: formikControlPropsType) => {
  const { control, inputProps } = props;

  switch (control) {
    case "text":  //it could be more cases like radio and so on , depending on the appkication
      return <ControlTexInput {...inputProps} />;
    default:
      return <></>;
  }
};

export default FormokControl;
