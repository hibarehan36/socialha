import { FormHelperText, TextField, TextFieldProps } from "@mui/material";
import { Box } from "@mui/system";
import { useField } from "formik";

const ControlTexInput = (props: TextFieldProps) => {
  const { name, ...other } = props;
  const [field, meta, helpers] = useField(name ?? "");

  return (
    <Box>
      <TextField
        {...other}
        value={field.value}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          helpers.setValue(event.target.value);
        }}
        margin="normal"
        className="search-field"
        fullWidth
        sx={{
          fontSize: "12px",
          "& .MuiOutlinedInput-notchedOutline": {
            borderWidth: "2px",
          },
        }}
      />
      <Box height={1}>
        {meta.error && <FormHelperText error>{meta.error}</FormHelperText>}
      </Box>
    </Box>
  );
};

export default ControlTexInput;
