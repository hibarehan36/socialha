import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Box,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { GenericPopupPropsType } from "../../types";

const Popup = (props: GenericPopupPropsType) => {
  const { isOpen, handleClose, children, title = "Recording" } = props;

  return (
    <Dialog open={isOpen} onClose={handleClose} className="popup-container">
      <DialogTitle align="center" className="popup-title">{title}</DialogTitle>
      <IconButton
        onClick={handleClose}
        sx={{
          position: "absolute",
          right: 8,
          top: 8,
          color: (theme) => theme.palette.grey[500],
        }}
      >
        <CloseIcon  className="popup-close-icon" />
      </IconButton>
      <DialogContent>{children[0]}</DialogContent>
      <Box display="flex" justifyContent="center" mb={1}>
        <DialogActions>{children[1]}</DialogActions>
      </Box>
    </Dialog>
  );
};

export default Popup;
