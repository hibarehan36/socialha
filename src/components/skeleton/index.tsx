import { Box, Skeleton } from "@mui/material";
const GridSkeleton = () => {
  return (
    <Box>
      {Array.from(Array(5)).map(() => (
        <Box mb={-3}>
          <Skeleton height={85} />
        </Box>
      ))}
    </Box>
  );
};

export default GridSkeleton;
