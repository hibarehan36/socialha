import { DataGrid } from "@mui/x-data-grid";
import CustomPagination from "../pagination";
import { GenericTablePropsType } from "../../types";

const Table = (props: GenericTablePropsType) => {
  const { paginationModel, setPaginationModel, ...other } = props;
  return (
    <DataGrid
      pagination
      paginationModel={paginationModel}
      onPaginationModelChange={setPaginationModel}
      {...other}
      pageSizeOptions={[5]}
      disableRowSelectionOnClick
      slots={{
        pagination: CustomPagination,
      }}
    />
  );
};

export default Table;
