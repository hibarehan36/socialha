import { postsRowType, postsType } from "../types";
import { accountImageSrc, mediaImageSrc } from "../context/imageProvider";
export const mapping = (rowData: postsRowType): postsType[] | [] => {
  if (!rowData) return [];
  const data = rowData.data.posts.data;
  const mappedData: postsType[] = data.map((post) => ({
    id: Number(post.id),
    postId: post.id,
    name: "person name",
    personImage: accountImageSrc,
    content: post.body,
    mediaUrl: mediaImageSrc,
    privacy: Number(post.id) % 2 === 0 ? "private" : "public",
  }));
  return mappedData;
};
