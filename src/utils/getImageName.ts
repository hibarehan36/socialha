export const getImageName = (url:string):string =>{
    const imageName =  url.split('/').pop()?.split('.')[0];
 return  imageName ?? "" ;
}