export const handelSearch = (
  event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  data: any[] | undefined,
  setFilterdRows: React.Dispatch<React.SetStateAction<any[] | undefined>>
) => {
  const searchKey = event.target.value;
  const filteredData = data?.filter((item) => {
    return Object.values(item).some((value) =>
      String(value).toLowerCase().includes(searchKey.toLowerCase())
    );
  });
  setFilterdRows(filteredData ?? [])
};
