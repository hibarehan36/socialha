import { actionInfoType } from "./gridActionTypes";
import { postsType } from "./postsTypes";

export type dialogProps = {
  actionInfo: actionInfoType;
  isOpen: boolean;
  resetDefault: () => void;
};

export type dialogContentPropsType = {
  resetDefault: () => void;
  row: postsType;
};

export type postCardHeaderProps = { personImage: string; name: string };
export type postCardBodyProps =  { mediaUrl: string } ;
