export type postsRowType = {
  data: {
    posts: {
      data: { id: string; body: string }[];
    };
  };
};

export type postsType = {
  id: number;
  postId: string;
  name: string;
  personImage: string;
  content: string;
  mediaUrl: string;
  privacy: "public"|"private";
};
