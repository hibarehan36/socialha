import { TextFieldProps } from "@mui/material";
import { FormikValues } from "formik";
import { ObjectSchema } from "yup";
import {GridColDef } from "@mui/x-data-grid";
//accordion types 
export type GenericAccordionPropsType = { expanded?: boolean; children: JSX.Element[] }

//formik types 
export type formikControlPropsType ={
inputProps: TextFieldProps;
control: string;
}
export type formikFormPropsType ={
    initialValues: FormikValues;
    onSubmit: (values: FormikValues) => void;
    validationSchema: ObjectSchema<any>;
    children: JSX.Element;
  }

//popup type 
export type GenericPopupPropsType ={
    isOpen: boolean;
    handleClose: () => void;
    title: string;
    children: JSX.Element[];
  }

  //searchField 
  export type GenericSearshFieldPropsType ={
    data: any[] | undefined;
    setFilterdRows: React.Dispatch<React.SetStateAction<any[] | undefined>>;
  }

  //  Table

  export type GenericTablePropsType ={
    rows: any[];
    columns: GridColDef[];
    paginationModel: {
      pageSize: number;
      page: number;
    };
    setPaginationModel: React.Dispatch<
      React.SetStateAction<{
        pageSize: number;
        page: number;
      }>
    >;
  }