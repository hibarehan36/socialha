import { postsType } from "./postsTypes";

type actionType = "view" | "edit" | "delete" ;
export type actionInfoType=  {
    row: postsType;
    actionType:actionType;
}

export type handelActionType =  (actionInfo: actionInfoType) => void ;