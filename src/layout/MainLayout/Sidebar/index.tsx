import { Box, CardMedia, Typography } from "@mui/material";
import { darkLogoSrc, postIconSrc } from "../../../context/imageProvider";
import Accordioin from "../../../components/accordion";
import SidebarItems from "./SidebarItems";

const Sidebar = (props: {
  setActiveLink: React.Dispatch<React.SetStateAction<string>>;
}) => {
  const { setActiveLink } = props;
  return (
    <Box className="sidebar-content" pl={2} >
      <Box display="flex" alignItems="center" gap={2}>
        <CardMedia
          component="img"
          src={darkLogoSrc}
          alt=""
          className="sidbar-logo-image"
        />
        <Typography
          variant="body1"
          fontWeight="bold"
          textTransform="capitalize"
          color="text.primary"
        >
          socialha
        </Typography>
      </Box>
      <Box mt={3} ml={-2}>
        <Accordioin>
          <Box display="flex" alignItems="center" ml={-1}  gap={1}>
            <CardMedia
              component="img"
              width="15px"
              height="25px"
              src={postIconSrc}
              alt=""
              className="sidbar-collaps-image"
            />
            <Typography variant="subtitle2" textTransform="capitalize" fontWeight="bold" color="secondary">
              posts
            </Typography>
          </Box>

          <Box display="flex" className="sidebar-items" alignItems="center" gap={1}>
            <SidebarItems setActiveLink={setActiveLink} />
          </Box>
        </Accordioin>
      </Box>
    </Box>
  );
};

export default Sidebar;
