import {
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
} from "@mui/material";

import FiberManualRecordIcon from "@mui/icons-material/FiberManualRecord";
import { NavLink as RouterLink } from "react-router-dom";
const SidebarItems = (props: {
  setActiveLink: React.Dispatch<React.SetStateAction<string>>;
}) => {
  const { setActiveLink } = props;
  return (
    <List>
      <ListItem
        disablePadding
        component={RouterLink}
        to="/statistics"
        className="sidebar-list-ltem"
      >
        <ListItemButton onClick={() => setActiveLink("Statistics")}>
          <ListItemIcon>
            <FiberManualRecordIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText
            primary={
              <Typography
                variant="subtitle2"
                textTransform="capitalize"
                color="text.dark"
              >
                statistics
              </Typography>
            }
          />
        </ListItemButton>
      </ListItem>
      <ListItem
        disablePadding
        component={RouterLink}
        to="/managements"
        className="sidebar-list-ltem"
      >
        <ListItemButton onClick={() => setActiveLink("Managements")}>
          <ListItemIcon>
            <FiberManualRecordIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText
            primary={
              <Typography
                variant="subtitle2"
                textTransform="capitalize"
                color="text.dark"
              >
                managements
              </Typography>
            }
          />
        </ListItemButton>
      </ListItem>
    </List>
  );
};

export default SidebarItems;
