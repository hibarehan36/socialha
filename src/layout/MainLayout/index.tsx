import React , {useState} from 'react'
import { Outlet } from "react-router-dom";
import { Grid, Box } from "@mui/material";
import Sidebar from "./Sidebar";
import Navbar from "./Navbar";
import Path from "./Path";
import '../../style/outlet.scss';
const MainLayout = () => {
  const [activeLink , setActiveLink] = useState<string>(window.location.pathname.slice(1));
  return (
    <Grid container alignItems="stretch" className="main-container">
      <Grid item md={2} xs={0} >
        <Box className="sidebar-container" sx={{ display: { xs: "none", md: "block" }, height: "100%" }}>
          <Sidebar setActiveLink={setActiveLink} />
        </Box>
      </Grid>
      <Grid item md={10} xs={12} >
        <Grid container alignItems="center">
          <Grid item xs={12}>
            <Navbar />
          </Grid>
          <Grid item xs={12}>
            <Box className="outlet-container">
              <Path activeLink={activeLink}/>
              <Outlet />
            </Box>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};
export default MainLayout;
