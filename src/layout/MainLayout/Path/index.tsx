import { Box, Typography } from "@mui/material";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
const Path = (props: { activeLink: string }) => {
  const { activeLink } = props;
  return (
    <Box display="flex" gap={2} mt={2} alignItems="center" >
      <Typography variant="subtitle2">Posts</Typography>
      <Box color="text.secondary" mt={"3px"}>
        <ArrowForwardIosIcon fontSize="small" />
      </Box>
      <Typography
        variant="subtitle2"
        textTransform="capitalize"
        color="secondary"
      >
        {activeLink}
      </Typography>
    </Box>
  );
};

export default Path;
