import React from "react";
import {
  AppBar,
  Toolbar,
  Box,
  Avatar,
  Stack,
  Badge,
  IconButton,
  CardMedia,
} from "@mui/material";
import {
  notificationIconSrc,
  settingIconSrc,
  accountImageSrc,
} from "../../../context/imageProvider";
import { useNavigate } from "react-router-dom";
import { logoutIconSrc } from "../../../context/imageProvider";

const Navbar = () => {
  const navigate = useNavigate();
  const handelLogout = () => {
    localStorage.clear();
    navigate("/");
    window.location.reload();
  };
  return (
    <Box className="nav-container">
      <AppBar position="static" elevation={1} >
        <Toolbar>
          <Box
            display="flex"
            width={"100%"}
            justifyContent="space-between"
            alignItems="center"
          >
            <Box>
              <IconButton onClick={handelLogout}>
                <CardMedia
                  component="img"
                  width={20}
                  height={20}
                  src={logoutIconSrc}
                  alt=""
                />
              </IconButton>
            </Box>
            <Stack direction="row" spacing={3} alignItems="center">
              <Badge badgeContent={1} color="error">
                <CardMedia
                  component="img"
                  width={20}
                  height={20}
                  src={notificationIconSrc}
                  alt=""
                />
              </Badge>
              <CardMedia
                component="img"
                width={20}
                height={20}
                src={settingIconSrc}
                alt=""
              />
              <Avatar
                alt=""
                src={accountImageSrc}
                sizes="small"
                className="nav-logo-profile-image"
              />
            </Stack>
          </Box>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Navbar;
