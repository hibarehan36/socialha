import React from "react";
import { Box, Typography, Stack, Button } from "@mui/material";
import { useGetHandelPostsApis } from "../../../../hooks/useGetHandelPostsApis";
import { dialogContentPropsType } from "../../../../types";

const DeleteContent = (props:dialogContentPropsType) => {
  const { resetDefault, row } = props;
  const { useDeletePost } = useGetHandelPostsApis();
  const { mutate: deletePost } = useDeletePost();
  const { postId } = row;
  return (
    <>
      <Box p={2}>
        <Typography variant="subtitle2" fontWeight="bold">Do you really want to delete this post? </Typography>
      </Box>
      <Stack spacing={2} direction="row" justifyContent="center">
        <Button
          variant="outlined"
          color="secondary"
          onClick={resetDefault}
          className="post-dialog-button"
        >
          cancel
        </Button>
        <Button
          variant="contained"
          color="secondary"
          className="post-dialog-button"
          onClick={() => {
            deletePost(postId);
            resetDefault();
          }}
        >
          delete
        </Button>
      </Stack>
    </>
  );
};

export default DeleteContent;
