import React from "react";
import { dialogProps } from "../../../../types";
import DeleteContent from "./DeleteContent";
import EditContent from "./EditContent";
import ViewContent from "./ViewContent";
import { Box } from "@mui/material";
const DialogContent = (props: dialogProps) => {
  const {actionInfo , resetDefault} = props;

  return (
    <Box className="popup-content">
      {actionInfo.actionType === "view" && <ViewContent row={actionInfo.row} resetDefault={resetDefault} />}
      {actionInfo.actionType === "edit" && <EditContent row={actionInfo.row} resetDefault={resetDefault}/>}
      {actionInfo.actionType === "delete" && <DeleteContent row={actionInfo.row} resetDefault={resetDefault}  />}
    </Box>
  );
};

export default DialogContent;
