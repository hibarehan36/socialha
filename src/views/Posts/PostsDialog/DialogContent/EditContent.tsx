import React from "react";
import { dialogContentPropsType} from "../../../../types";
import { useGetHandelPostsApis } from "../../../../hooks/useGetHandelPostsApis";
import { Stack, Button, Box,Paper } from "@mui/material";
import PostCardHeader from "../PostCard/PostCardHeader";
import PostCardBody from "../PostCard/PostCardBody";
import FormokControl from "../../../../components/formik/FormikControl";
import FormikFrom from "../../../../components/formik/FormikFrom";
import { FormikValues } from "formik";
import { useGetValidationSchema } from "../../../../hooks/useGetValidationSchema";

const EditContent = (props:dialogContentPropsType) => {
  const { resetDefault, row } = props;
  const { useUpdatePost } = useGetHandelPostsApis();
  const { mutate: updatePost } = useUpdatePost();
  const { postId, personImage, name, mediaUrl , content} = row;
  const { postValidationSchema } = useGetValidationSchema();
  return (
    <>
     
      <FormikFrom
        initialValues={{
          content: content,
        }}
        onSubmit={(values: FormikValues) => {
          updatePost({ postId: postId, updatedPost: values.content });
          resetDefault();
        }}
        validationSchema={postValidationSchema}
      >
        <>
        <Paper elevation={2}> 
          <PostCardHeader personImage={personImage} name={name} />
          <Box p={2} pt={0} pb={0}>
            <FormokControl
              control="text"
              inputProps={{
                type: "text",
                name: "content",
              }}
            />
          </Box>
          <PostCardBody mediaUrl={mediaUrl} />
          </Paper>
          <Stack mt={1}  spacing={2} direction="row" justifyContent="center">
            <Button
              variant="outlined"
              color="secondary"
              type="button"
              onClick={resetDefault}
              className="post-dialog-button"
            >
              cancel
            </Button>
            <Button
              variant="contained"
              color="secondary"
              className="post-dialog-button"
              type="submit"
            >
              edit
            </Button>
          </Stack>
        </>
      </FormikFrom>
     
    </>
  );
};

export default EditContent;
