import React from "react";
import { dialogContentPropsType } from "../../../../types";
import { Stack, Button, Box, Typography, Paper } from "@mui/material";
import PostCardHeader from "../PostCard/PostCardHeader";
import PostCardBody from "../PostCard/PostCardBody";

const ViewContent = (props: dialogContentPropsType) => {
  const { resetDefault, row } = props;
  const { personImage, name, mediaUrl, content } = row;
  return (
    <>
      <Paper elevation={2}>
      <PostCardHeader personImage={personImage} name={name} />
      <Box p={2} pb={1}>
        <Typography variant="caption" textTransform="capitalize" lineHeight={"5px"}>
          {content}
        </Typography>
      </Box>
        <PostCardBody mediaUrl={mediaUrl} />
      </Paper>
        <Stack mt={1}  spacing={2} direction="row" justifyContent="center">
          <Button
            variant="outlined"
            color="secondary"
            type="button"
            onClick={resetDefault}
            className="post-dialog-button"
          >
            cancel
          </Button>
        </Stack>
    </>
  );
};

export default ViewContent;
