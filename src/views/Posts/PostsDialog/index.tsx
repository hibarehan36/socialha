import React from "react";
import Popup from "../../../components/popup";
import { dialogProps } from "../../../types";
import DialogContent from "./DialogContent";

const PostDialog = (props: dialogProps) => {
  const { actionInfo, isOpen, resetDefault } = props;
  return (
    <Popup
      title={actionInfo.actionType === "delete" ? "" : `${actionInfo.actionType}  posts`}
      isOpen={isOpen}
      handleClose={resetDefault}
    >
      <DialogContent {...props} />
      <></>
    </Popup>
  );
};

export default PostDialog;
