import React from "react";
import { CardMedia, Box, Typography, Stack } from "@mui/material";
import { postCardBodyProps } from "../../../../types";
import { LikeIconIconSrc } from "../../../../context/imageProvider";

const PostCardBody = (props: postCardBodyProps) => {
  const { mediaUrl } = props;
  return (
    <>
      <Box p={2} pt={0}>
        <CardMedia
          sx={{ borderRadius: "5px" }}
          component="img"
          height="200"
          image={mediaUrl}
          alt=""
        />
      </Box>
      <Stack ml={3} mt={-1} spacing={1} pb={2} direction="row" alignItems="center">
      <CardMedia
          sx={{ borderRadius: "5px" }}
          component="img"
          className="heart-icon"
          image={LikeIconIconSrc}
          alt=""
        />
        <Typography variant="caption" ml={-1}>23</Typography>
      </Stack>
    </>
  );
};

export default PostCardBody;
