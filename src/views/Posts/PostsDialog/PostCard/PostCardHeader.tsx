import React from "react";
import { CardHeader, Avatar, Typography, Box } from "@mui/material";
import { postCardHeaderProps } from "../../../../types";

const PostCardHeader = (props: postCardHeaderProps) => {
  const { personImage, name } = props;
  return (
    <CardHeader
    sx={{marginBottom:"-20px"}}
      avatar={<Avatar src={personImage} />}
      title={
        <Typography variant="subtitle2" textTransform="capitalize" fontWeight="bold">
          {name}
        </Typography>
      }
      subheader={
        <Box>
        <Typography variant="caption" color={"text.secondary"} textTransform="capitalize">
          12 Aug 2022 10:00 pm
        </Typography>
        </Box>
      }
    />
  );
};

export default PostCardHeader;
