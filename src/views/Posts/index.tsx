import  { useState } from "react";
import Table from "../../components/table";
import { UseGetGridColumns } from "../../hooks/useGetGridColumns";
import { Box, Paper } from "@mui/material";
import SearchField from "../../components/searchField";
import PostDialog from "./PostsDialog";
import { UseHandelGridActions } from "../../hooks/useHandelGridActions";
import { useGetHandelPostsApis } from "../../hooks/useGetHandelPostsApis";
import GridSkeleton from "../../components/skeleton";
const Posts = () => {
  const { action, handelAction, isOpen, resetDefault } = UseHandelGridActions();
  const gridColumns = UseGetGridColumns(handelAction);
  const [paginationModel, setPaginationModel] = useState({
    pageSize: 5,
    page: 0,
  });
  const { useFetchPosts } = useGetHandelPostsApis();
  const { data, isLoading, isError } = useFetchPosts();

  const [filterdRows, setFilterdRows] = useState<any[] | undefined>(data);
  if (isError) return <Box>someThing went worong</Box>;
  return (
    <Box className="posts-container">
      <SearchField data={data ?? []} setFilterdRows={setFilterdRows} />
      {isLoading || !data ? (
        <GridSkeleton />
      ) : (
        <>
          <Paper elevation={0} className="posts-paper" >
            <Table
              rows={filterdRows ?? data}
              columns={gridColumns}
              paginationModel={paginationModel}
              setPaginationModel={setPaginationModel}
            />
          </Paper>
          <PostDialog
            actionInfo={action}
            isOpen={isOpen}
            resetDefault={resetDefault}
          />
        </>
      )}
    </Box>
  );
};

export default Posts;
