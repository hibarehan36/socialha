import React from 'react'
import { Box } from "@mui/material";
import Posts from '../Posts';

const Home = () => {
  return (
   <Box mt={4}>
    <Posts/>
   </Box>
  )
}

export default Home;
