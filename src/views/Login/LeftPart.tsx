import { Box, Typography,CardMedia } from "@mui/material";
import { loginImageSrc , lightLogoSrc, sloganSrc } from "../../context/imageProvider";

const LeftPart = () => {
  return (
    <Box className="login-left-part" p={3}>
      <Box
        className="login-logo"
        display="flex"
        alignItems="center"
        gap={1}
        ml={3}
      >
        <CardMedia
        className="login-logo--image"
          component="img"
          image={lightLogoSrc}
          alt=""
        />
        <Typography variant="body2" color="primary" textTransform="capitalize">
          socialha
        </Typography>
      </Box>
      <Box className="images-part" >
      <Box className="login-title" mt={3} ml={-2}>
         <CardMedia
        className="login-solgan-image"
          component="img"
          image={sloganSrc}
          alt=""
        />
      </Box>
      <Box className="login-image" mt={3} >
        <img src={loginImageSrc} alt="" className="image"/>
      </Box>
      </Box>
    </Box>
  );
};

export default LeftPart;
