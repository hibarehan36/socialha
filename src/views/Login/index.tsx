import { Box } from "@mui/material";
import React from "react";
import LeftPart from "./LeftPart";
import RightPart from "./RightPart";

const Login = () => {  
  return (
    <Box className="login-container" display="flex">
      <Box className="login-overlay" display="flex">
        <Box className="login-white-part"></Box>
      </Box>
      <Box className="login-content" display="flex">
        <LeftPart />
        <RightPart />
      </Box>
    </Box>  
  );
};

export default Login;
