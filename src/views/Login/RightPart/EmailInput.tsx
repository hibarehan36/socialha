
import FormokControl from "../../../components/formik/FormikControl";
import { InputAdornment } from "@mui/material";
import { emailIconSrc } from "../../../context/imageProvider";

const EmailInput = () => {
  return (
    <FormokControl
    control="text"
    inputProps={{
      type: "text",
      variant:"filled",
      placeholder: "Please enter your email",
      name: "email",
      InputProps: {
        startAdornment: (
          <InputAdornment position="start">
            <img src={emailIconSrc} width={20} alt="" />
          </InputAdornment>
        ),
      },
    }}
  />
  )
}

export default EmailInput
