import { Box, Typography } from "@mui/material";

import React from "react";

const Header = () => {
  return (
    <Box>
      <Typography variant="h5" color="text" textAlign="center" fontWeight="bold">
        Welcome to Socialha 👋🏼
      </Typography>
      <Typography  variant="subtitle2" gutterBottom textAlign="center" mt={1}>
        Please enter your information below
      </Typography>
    </Box>
  );
};

export default Header;
