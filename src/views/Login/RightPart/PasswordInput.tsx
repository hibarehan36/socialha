import { useState } from "react";
import FormokControl from "../../../components/formik/FormikControl";
import {InputAdornment } from "@mui/material";
import {passwordIconSrc} from "../../../context/imageProvider"
import IconButton from "@mui/material/IconButton";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";


const PasswordInput = () => {
    const [showPassword, setShowPassword] = useState<Boolean>(false);
  return (
    <FormokControl
    control="text"
    inputProps={{
      type: showPassword ? "text" : "password",
      name: "password",
      variant:"filled",
      placeholder: "Please enter your password",
      InputProps: {
        startAdornment: (
          <InputAdornment position="start">
            <img src={passwordIconSrc} width={20} alt="" />
          </InputAdornment>
        ),
        endAdornment: (
          <InputAdornment position="end">
            <IconButton
              onClick={() => setShowPassword((show) => !show)}
              edge="end"
            >
              {showPassword ? <VisibilityOff /> : <Visibility />}
            </IconButton>
          </InputAdornment>
        ),
      },
    }}
  />
  )
}

export default PasswordInput
