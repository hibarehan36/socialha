import { Box, Button, Typography } from "@mui/material";
import FormikFrom from "../../../components/formik/FormikFrom";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { useGetValidationSchema } from "../../../hooks/useGetValidationSchema";
import { FormikValues } from "formik";
import PasswordInput from "./PasswordInput";
import EmailInput from "./EmailInput";
import { useNavigate } from "react-router-dom";

const LoginForm = () => {
  const { loginValidationSchema } = useGetValidationSchema();
  const navigate = useNavigate();
  return (
    <Box width={"100%"} mt={3} maxWidth={350}>
      <FormikFrom
        initialValues={{
          email: "",
          password: "",
        }}
        onSubmit={(values: FormikValues) => {
          localStorage.setItem("token", "signed up");
          navigate("/");
          window.location.reload();
        }}
        validationSchema={loginValidationSchema}
      >
        <>
          <EmailInput />
          <PasswordInput />
          <Box
            className="login-checkbox"
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            mt={-1}
          >
            <FormGroup>
              <FormControlLabel
                control={<Checkbox className="checkbox" />}
                label={
                  <Typography variant="caption" fontSize={"13px"}>
                    Remember me
                  </Typography>
                }
              />
            </FormGroup>
            <Typography variant="caption" color="secondary">
              forget password?
            </Typography>
          </Box>
          <Box mt={2}>
            <Button
              variant="contained"
              className="login-button"
              fullWidth
              color="secondary"
              type="submit"
            >
              sign in
            </Button>
          </Box>
        </>
      </FormikFrom>
    </Box>
  );
};

export default LoginForm;
