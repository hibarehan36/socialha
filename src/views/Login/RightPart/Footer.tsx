import { Box, Typography } from "@mui/material";
import React from "react";

const Footer = () => {
  return (
    <Box display="flex" alignItems="center" justifyContent="center" mt={3}>
      <Typography     variant="subtitle2" gutterBottom textAlign="center">
        Don’t you have an account?
      </Typography>
      <Typography
       variant="subtitle2"
        gutterBottom
        fontWeight="bold"
        color="secondary"
        ml={1}
      >
        Sign up
      </Typography>
    </Box>
  );
};

export default Footer;
