import { Box } from "@mui/material";
import React from "react";
import LoginForm from "./LoginForm";
import Footer from "./Footer";
import Header from "./Header";

const RightPart = () => {
  return (
    <Box
      className="login-right-part"
      p={3}
      display="flex"
      flexDirection="column"
      alignItems="center"
     justifyContent="center"
    >
      <Header />
      <LoginForm />
      <Footer />
    </Box>
  );
};

export default RightPart;
